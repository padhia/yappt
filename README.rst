yappt
=====

.. image:: https://img.shields.io/pypi/v/yappt.svg
     :target: https://pypi.python.org/pypi/yappt
     :alt: PyPi
.. image:: https://img.shields.io/badge/License-MIT-blue.svg
     :target: https://opensource.org/licenses/MIT
     :alt: MIT License
.. image:: https://img.shields.io/pypi/pyversions/yappt.svg
     :alt: Python3.6+

Yet Another Pretty Printer for Tables or Trees

`yappt <https://bitbucket.org/padhia/yappt>`_ is a collection of functions and classes to pretty print tabular and tree-form data.

Development of this module was inspired by https://bitbucket.org/astanin/python-tabulate
